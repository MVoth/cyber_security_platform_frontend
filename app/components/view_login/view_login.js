'use strict';

angular.module('cscFrontend.view_login', ['ngRoute','cscFrontend.service_authentication'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view_login', {
            templateUrl: 'components/view_login/view_login.html',
            controller: 'ViewLoginController',
            controllerAs: "loginController",
            noAuth: true
        });
    }])

    .controller('ViewLoginController', function($location, $scope, AuthenticationService, NotificationService) {
        return {
            login: function(user) {
                AuthenticationService.login(user.username, user.password)
                    .then(null, function(response) {
                        NotificationService.danger("You have entered an invalid username or password.");
                    });
            }
        }
    });