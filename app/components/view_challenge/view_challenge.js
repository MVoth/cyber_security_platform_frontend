'use strict';

angular.module('cscFrontend.view_challenge', [
    'cscFrontend.service_notification',
    'cscFrontend.model_challenge',
    'cscFrontend.model_challengeUserDetails',
    'cscFrontend.model_challengeUserPoints',
    'cscFrontend.model_challengeHint',
    'cscFrontend.model_challengeHintPointReduction',
    'cscFrontend.model_challengeDescription'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view_challenge/:challengeId', {
            templateUrl: 'components/view_challenge/view_challenge.html',
            controller: 'ViewChallengeController',
            controllerAs: 'challengeController',
            requireAuth: true
        });
    }])

    .controller('ViewChallengeController', function (config, Challenge, ChallengeDescription, ChallengeUserDetails, ChallengeUserPoints, ChallengeUserSolvingPoints, ChallengeHint, ChallengeHintPointReduction, SessionService, NotificationService, $routeParams, $http) {
        var self = this
        this.challenge = Challenge.get({challengeId: $routeParams.challengeId});
        this.description = ChallengeDescription.get({challengeId: $routeParams.challengeId});
        this.hints = [];
        this.hintAccessed = function(hintId) {
            var tmp = false;
            angular.forEach(this.userDetails.HintAccess, function(access) {
                if(hintId === access.HintId) tmp = true;
            });
            return tmp;
        };

        this.selectHint = function(hintId) {
            self.selectedHint = self.hints[hintId-1];
        }
        this.receiveHint = function() {
            self.hints[self.selectedHint.index].hint = ChallengeHint.get({challengeId: $routeParams.challengeId, hintId: this.selectedHint.ID});
        };

        this.refreshUserDetails = function() {
            self.userDetails = ChallengeUserDetails.get({userId: SessionService.getUser().Id, challengeId: $routeParams.challengeId}, function() {
                $http.get(config.apiUrl + "/challenges/" + $routeParams.challengeId+ "/hints")
                    .then(function(response) {
                        var i=0;
                        angular.forEach(response.data, function(h) {
                            self.hints[i] = {hint: null, pointreduction: null};
                            if(self.hintAccessed(h)) {
                                self.hints[i].hint = ChallengeHint.get({challengeId: $routeParams.challengeId, hintId: h});
                            } else {
                                self.hints[i].pointreduction = ChallengeHintPointReduction.get({challengeId: $routeParams.challengeId, hintId: h});
                                self.hints[i].ID = h;
                                self.hints[i].index = i;
                            }
                            i++;
                        })
                    });
            });
            self.userPoints = ChallengeUserPoints.get({userId: SessionService.getUser().Id, challengeId: $routeParams.challengeId});
            self.userSolvingPoints = ChallengeUserSolvingPoints.get({userId: SessionService.getUser().Id, challengeId: $routeParams.challengeId});
        };
        this.refreshUserDetails();

        this.submitSolution = function(data) {
            $http.post(config.apiUrl + "/challenges/" + $routeParams.challengeId + "/solution", {
                Solution: data.solution
            }).then(function() {
                NotificationService.success("Congratulations, you solved the challenge.");
                angular.element('#passwordChangeModal').modal('hide');
            }, function(response) {
                NotificationService.danger("Sorry, but your solution is not correct.");
                self.refreshUserDetails();
            });
        }
    });