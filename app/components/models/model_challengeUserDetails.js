angular.
module('cscFrontend.model_challengeUserDetails', ['ngResource']).
factory('ChallengeUserDetails', function(config, $resource) {
        return $resource(config.apiUrl + '/users/:userId/challenges/:challengeId', {}, {
        });
    }
);