angular.
module('cscFrontend.model_event', ['cscFrontend.model_challengeset','ngResource']).
factory('Event', function(ChallengeSet, config, $q, $resource) {
        var Event = $resource(config.apiUrl + '/events/:eventId', {}, {
            userEvents: {
                method: 'GET',
                isArray: true,
                url: config.apiUrl + '/user/events'
            },
            ranking: {
                method:'GET',
                isArray: true,
                url: config.apiUrl + '/events/:eventId/ranking'
            }
        });
        return Event;
    }
);