angular.
module('cscFrontend.model_user', ['ngResource']).
factory('User', ['config', '$resource',
    function(config, $resource) {
        return $resource(config.apiUrl + '/users/:userId', {}, {
            authenticatedUser: {
                method: 'GET',
                url: config.apiUrl + '/user'
            }
        });
    }
]);