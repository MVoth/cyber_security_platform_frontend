angular.
module('cscFrontend.model_challengeUserSolvingPoints', ['ngResource']).
factory('ChallengeUserSolvingPoints', function(config, $resource) {
        return $resource(config.apiUrl + '/users/:userId/challenges/:challengeId/pointsifsolvednow', {}, {
        });
    }
);