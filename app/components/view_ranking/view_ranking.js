'use strict';

angular.module('cscFrontend.view_ranking', [
    'ngRoute',
    'cscFrontend.model_event',
    'cscFrontend.model_challengeset',
    'cscFrontend.model_challengeUserPoints'
])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view_ranking/:eventId', {
            templateUrl: 'components/view_ranking/view_ranking.html',
            controller: 'ViewRankingController',
            controllerAs: 'rankingController',
            requireAuth: true
        });
    }])

    .controller('ViewRankingController', function (Event, ChallengeSet, $routeParams) {
        this.event = Event.get({eventId: $routeParams.eventId});
        this.eventRanking = Event.ranking({eventId: $routeParams.eventId});
        this.getUserPointsBreakdown = function(user) {
            user.ChallengeSets = [];
            angular.forEach(this.event.ChallengeSets, function(setInfo) {
                ChallengeSet.get({challengeSetId: setInfo.ChallengeSetId}, function(set) {
                    user.ChallengeSets.push(set);
                });
            })
        };
    });