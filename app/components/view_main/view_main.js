'use strict';

angular.module('cscFrontend.view_main', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view_main', {
            templateUrl: 'components/view_main/view_main.html',
            controller: 'ViewMainController',
            requireAuth: true
        });
    }])

    .controller('ViewMainController', [function () {

    }]);