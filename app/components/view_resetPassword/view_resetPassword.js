'use strict';

angular.module('cscFrontend.view_resetPassword', ['ngRoute', 'ui.validate'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view_resetPassword/', {
            templateUrl: 'components/view_resetPassword/view_resetPassword.html',
            controller: 'ViewResetPasswordController',
            controllerAs: "resetPasswordController",
            noAuth: true
        });
    }])

    .controller('ViewResetPasswordController', function (config, $scope, $routeParams, $http, NotificationService) {
        this.reset = function(reset) {
            $http.post(config.apiUrl + "/users/resetpassword", {

            }).then(function() {
                NotificationService.success("Your password has been changed. Proceed to login...");
                $scope.resetSuccess = true;
            }, function(response) {
                NotificationService.danger("You have entered an invalid e-mail address or reset code.");
            });
        }
    });