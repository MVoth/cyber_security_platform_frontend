'use strict';

angular.module('cscFrontend.view_welcome', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view_welcome', {
            templateUrl: 'components/view_welcome/view_welcome.html',
            controller: 'ViewWelcomeController',
            noAuth: true
        });
    }])

    .controller('ViewWelcomeController', [function () {

    }]);